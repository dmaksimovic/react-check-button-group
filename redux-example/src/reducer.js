import {ActionType} from './action-type';

var initialState = {
    numbers: 'Two'
};

const REDUX_INTERNAL_INIT = "@@redux/INIT";
const ignore = () => {};

export default function(state = initialState, action) {
    let newState = state;

    switch (action.type) {
        case ActionType.SetNumbers:
            newState = {
                ...state,
                numbers: action.data
            };
            break;
        case REDUX_INTERNAL_INIT:
            ignore();
            break;
        default:
            console.warn("Unknown action in reducer: ", action);
            break;
    }

    return newState;
}
