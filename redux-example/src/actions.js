import {ActionType} from './action-type';

export const setNumbers = (numbers) => {
    return {
        type: ActionType.SetNumbers,
        data: numbers
    };
};
